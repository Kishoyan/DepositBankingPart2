﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DepositBanking.Repository.DepositBankingEntityModels;
using AutoMapper;
using DepositBanking.Repository.Deposits;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;
using DepositBanking.Repositories;

namespace Volo.Luiza_Kishoyan.DepositBanking.Controllers
{
    public class DepositorsController : BaseController
    {
        public ActionResult Invest(DepositorCreateViewModel depositor)
        {
            if (depositor == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DepositorInvestViewModel investment = new DepositorInvestViewModel();

            var res = depositBankingRepository.GetDepositorByEmail(depositor.Email);
            Depositor newDepositor = new Depositor();
            if (res == null)
            {
                Mapper.Initialize(b => b.CreateMap<DepositorCreateViewModel, Depositor>()
                         .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now)));
                newDepositor = Mapper.Map<DepositorCreateViewModel, Depositor>(depositor);
                depositBankingRepository.Create<Depositor>(newDepositor);

                investment.DepositorId = depositBankingRepository.GetDepositorByEmail(depositor.Email).Id;
            }
            else
            {
                investment.DepositorId = res.Id;
            }
            investment.Depositor = newDepositor;
            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Currencies = new List<CurrencyInfo>();
            return View(investment);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Invest(DepositorInvestViewModel investment)
        {
           if (ModelState.IsValid)
            {               
                    Mapper.Initialize(b => b.CreateMap<DepositorInvestViewModel, Investment>()
                        .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now))
                        .ForMember("FinalAmount", opt => opt.MapFrom(d => d.Amount + d.Duration * ((d.Rate / 100) / 12) * d.Amount))
                        .ForMember("BankState", opt => opt.MapFrom(d => "Active"))
                        .ForMember("DepositorState", opt => opt.MapFrom(d => "Active")));
                    Investment newInvestment = Mapper.Map<DepositorInvestViewModel, Investment>(investment);                               

                depositBankingRepository.Create<Investment>(newInvestment);
                return RedirectToAction("Index","Home");
            }

            return View(investment);
        }
       
    }
}
