﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DepositBanking.Repository.IdentityModels;
using Volo.Luiza_Kishoyan.DepositBanking.ViewModels.AccountViewModels;
using AutoMapper;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Controllers
{
    public class AccountController : Areas.Admin.Controllers.BaseController
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(AccountRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<AccountRegisterViewModel, ApplicationUser>()
                    .ForMember("UserName", opt => opt.MapFrom(n => n.Email))
                    .ForMember("PhoneNumber", opt => opt.MapFrom(n => n.Phone))
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now)));

                ApplicationUser user = Mapper.Map<AccountRegisterViewModel, ApplicationUser>(model);
                
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
               
                if (result.Succeeded)
                {
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
             }
            return View(model);
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "Incorrect Login and Password");
                }
                else
                {
                    Session["UserID"] = user.Id.ToString();
                    Session["UserName"] = user.UserName.ToString();

                    DepositorCreateViewModel newDepositor = new DepositorCreateViewModel()
                    {
                        Name = user.Name,
                        Surname = user.Surname,
                        Address = user.Address,
                        Phone = user.PhoneNumber,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        Gender = user.Gender
                    };
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Invest", "Depositors", newDepositor);
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
           
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            Session["UserName"] = null;
            return RedirectToAction("Index","Home");
        }

    }
}