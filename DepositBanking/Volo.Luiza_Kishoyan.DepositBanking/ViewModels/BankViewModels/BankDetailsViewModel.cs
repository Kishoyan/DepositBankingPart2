﻿using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Volo.Luiza_Kishoyan.DepositBanking.ViewModels.BankViewModels
{
    public class BankDetailsViewModel
    {

      public BankDetailsViewModel()
        {
            Investments = new HashSet<Investment>();
            Depositors = new HashSet<Depositor>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string OrganizationHead { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

        public int? Branch { get; set; }

        [Required]
        [StringLength(100)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string SWIFT { get; set; }

        [Required]
        [StringLength(20)]
        public string TPIN { get; set; }

        [StringLength(20)]
        public string Logo { get; set; }

        public DateTime CreationDate { get; set; }

        public string State { get; set; }

        public virtual ICollection<Investment> Investments { get; set; }
     
        public virtual ICollection<Depositor> Depositors { get; set; }
    }
}