﻿using DepositBanking.Repository.Deposits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Luiza_Kishoyan.DepositBanking.ViewModels.HomeViewModels
{
    public class CalculatorViewModel
    {
        public string Type { get; set; }

        public double Rate { get; set; }

        public int Duration { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }

        public double FinalAmount { get; set; }

        public IEnumerable<DepositInfo> Deposits { get; set; }

        public IEnumerable<CurrencyInfo> Currencies { get; set; }
    }
}