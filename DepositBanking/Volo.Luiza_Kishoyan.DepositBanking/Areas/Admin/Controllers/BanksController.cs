﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DepositBanking.Repository.DepositBankingEntityModels;
using System.IO;
using AutoMapper;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    public class BanksController : BaseController
    {
        public ActionResult Index()
        {
            Mapper.Initialize(b => b.CreateMap<Bank, BankIndexViewModel>());
            var banks = Mapper.Map<IEnumerable<Bank>, IEnumerable<BankIndexViewModel>>(depositBankingRepository.GetAll<Bank>());

            return View(banks);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Bank, BankDetailsViewModel>()
                .ForMember("State", opt => opt.MapFrom(n => n.DestroyDate == null ? "Active" : "Not Active")));
            BankDetailsViewModel bank = Mapper.Map<Bank, BankDetailsViewModel>(depositBankingRepository.GetById<Bank>(id.Value));

            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,OrganizationHead,Address,Branch,Phone,Fax,Email,SWIFT,TPIN")] BankCreateViewModel bank, HttpPostedFileBase Logo)
        {
            if (ModelState.IsValid)
            {
                string fileName = "";
                if (Logo != null)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/BankLogos"), Logo.FileName);
                    fileName = Path.GetFileNameWithoutExtension(Logo.FileName);
                    Logo.SaveAs(path);
                }
                else
                {
                    fileName = null;
                }
                Mapper.Initialize(b => b.CreateMap<BankCreateViewModel, Bank>()
                    .ForMember("Logo", opt => opt.MapFrom(n => fileName))
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now)));

                Bank newbank = Mapper.Map<BankCreateViewModel, Bank>(bank);

                depositBankingRepository.Create<Bank>(newbank);
                return RedirectToAction("Index");
            }
            return View(bank);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Bank, BankEditViewModel>());
            BankEditViewModel bank = Mapper.Map<Bank, BankEditViewModel>(depositBankingRepository.GetById<Bank>(id.Value));

            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BankEditViewModel bank, HttpPostedFileBase Logo)
        {
            if (ModelState.IsValid)
            {
                string fileName = "";
                if (Logo != null)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/BankLogos"), Logo.FileName);
                    fileName = Path.GetFileNameWithoutExtension(Logo.FileName);
                    Logo.SaveAs(path);
                }
                else
                {
                    fileName = null;
                }
                Mapper.Initialize(b => b.CreateMap<BankEditViewModel, Bank>()
                    .ForMember("Logo", opt => opt.MapFrom(n => fileName)));
                Bank newbank = Mapper.Map<BankEditViewModel, Bank>(bank);

                depositBankingRepository.Update<Bank>(newbank);
                return RedirectToAction("Index");
            }
            return View(bank);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Bank, BankDetailsViewModel>()
                .ForMember("State", opt => opt.MapFrom(n => n.DestroyDate == null ? "Active" : "Not Active")));
            BankDetailsViewModel bank = Mapper.Map<Bank, BankDetailsViewModel>(depositBankingRepository.GetById<Bank>(id.Value));

            if (bank == null)
            {
                return HttpNotFound();
            }

            return View(bank);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bank bank = depositBankingRepository.GetById<Bank>(id);

            if (bank == null)
            {
                return HttpNotFound();
            }
            else
            {
                bank.DestroyDate = DateTime.Now;
            }

            depositBankingRepository.Update<Bank>(bank);

            var investments = depositBankingRepository.GetAll<Investment>().Where(b => b.BankId == id).ToList();
            if (investments == null)
            {
                return HttpNotFound();
            }
            else
            {
                foreach (var item in investments)
                {
                    item.BankState = "Not Active";
                    depositBankingRepository.Update<Investment>(item);
                }
            }
            return RedirectToAction("Index");
        }
    }
}
