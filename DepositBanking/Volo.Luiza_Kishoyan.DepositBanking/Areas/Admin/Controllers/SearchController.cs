﻿using AutoMapper;
using DepositBanking.Repository.Deposits;
using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.SearchViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    public class SearchController : BaseController
    {
        public ActionResult Search()
        {
            SearchViewModel searchModel = new SearchViewModel();
            searchModel.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            searchModel.Depositors = DepositorsDropdownListCreater(depositBankingRepository.GetAllActiveDepositors());
            searchModel.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            
            searchModel.Bank = searchModel.Banks.FirstOrDefault().Name;
            searchModel.Depositor = searchModel.Depositors.FirstOrDefault().FullName;
            searchModel.Deposit = searchModel.Deposits.FirstOrDefault().Type;
            
            return View(searchModel);
        }

        [HttpPost]
        public ActionResult SearchResults(SearchViewModel searchModel)
        {
            //searchModel.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            //searchModel.Depositors = DepositorsDropdownListCreater(depositBankingRepository.GetAllActiveDepositors());
            //searchModel.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);

            var Results = GetSearchResults(searchModel);

            return PartialView(Results);
        }


        public List<SearchResultsViewModel> GetSearchResults(SearchViewModel searchModel)
        {
            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentIndexViewModel>()
                .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
                .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name + " " + n.Depositor.Surname)));
            var investments =Mapper.Map<IEnumerable<Investment>, IEnumerable<InvestmentIndexViewModel>>(depositBankingRepository.GetAll<Investment>()).AsQueryable();

            List<SearchResultsViewModel> results = new List<SearchResultsViewModel>();
            if (searchModel != null)
            {
                if (string.IsNullOrEmpty(searchModel.Bank) && string.IsNullOrEmpty(searchModel.Depositor) && string.IsNullOrEmpty(searchModel.Deposit) &&
                        !searchModel.AmountFrom.HasValue && !searchModel.AmountTo.HasValue && !searchModel.StartDateFrom.HasValue && !searchModel.StartDateTo.HasValue)
                    {
                        return results;
                    } 
                
                if (!string.IsNullOrEmpty(searchModel.Bank))
                    investments = investments.Where(x => x.Bank.Contains(searchModel.Bank));
                if (!string.IsNullOrEmpty(searchModel.Depositor))
                    investments = investments.Where(x => x.Depositor.Contains(searchModel.Depositor));
                if (!string.IsNullOrEmpty(searchModel.Deposit))
                    investments = investments.Where(x => x.Type.Contains(searchModel.Deposit));
                if (searchModel.AmountFrom.HasValue)
                    investments = investments.Where(x => x.Amount >= searchModel.AmountFrom);
                if (searchModel.AmountTo.HasValue)
                    investments = investments.Where(x => x.Amount <= searchModel.AmountTo);
                if (searchModel.StartDateFrom.HasValue)
                    investments = investments.Where(x => x.StartDate >= searchModel.StartDateFrom);
                if (searchModel.StartDateTo.HasValue)
                    investments = investments.Where(x => x.StartDate <= searchModel.StartDateTo);

                foreach (var item in investments)
                {
                    var res = new SearchResultsViewModel()
                    {
                        DepositorName = item.Depositor,
                        BankName = item.Bank,
                        DepositType = item.Type
                    };

                    results.Add(res);
                }
            }         
            return results;
        }

        
    }
}