﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using DepositBanking.Repository.DepositBankingEntityModels;
using AutoMapper;
using DepositBanking.Repository.Deposits;
using PagedList;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{  
    public class InvestmentsController : BaseController
    {
        public ActionResult Index(int? page)
        {
            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentIndexViewModel>()
                .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
                .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name + " " + n.Depositor.Surname)));
            var investments = Mapper.Map<IEnumerable<Investment>, IEnumerable<InvestmentIndexViewModel>>(depositBankingRepository.GetAll<Investment>());
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(investments.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentDetailsViewModel>()
                .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
                .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name+" "+n.Depositor.Surname)));
            InvestmentDetailsViewModel investment = Mapper.Map<Investment, InvestmentDetailsViewModel>(depositBankingRepository.GetById<Investment>(id.Value));

            if (investment == null)
            {
                return HttpNotFound();
            }
            return View(investment);
        }

        public ActionResult Create()
        {
            InvestmentCreateViewModel investment = new InvestmentCreateViewModel();
            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Depositors = DepositorsDropdownListCreater(depositBankingRepository.GetAllActiveDepositors());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Currencies = new List<CurrencyInfo>();

            return View(investment);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InvestmentCreateViewModel investment)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<InvestmentCreateViewModel, Investment>()
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now))
                    .ForMember("FinalAmount", opt => opt.MapFrom(d => d.Amount+d.Duration*((d.Rate/100)/12)*d.Amount))
                    .ForMember("BankState", opt => opt.MapFrom(d => "Active"))
                    .ForMember("DepositorState", opt => opt.MapFrom(d => "Active")));
                Investment newInvestment = Mapper.Map<InvestmentCreateViewModel, Investment>(investment);
                
                depositBankingRepository.Create<Investment>(newInvestment);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentEditViewModel>());
            InvestmentEditViewModel investment = Mapper.Map<Investment, InvestmentEditViewModel>(depositBankingRepository.GetById<Investment>(id.Value));

            if (investment == null)
            {
                return HttpNotFound();
            }
            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Depositors = DepositorsDropdownListCreater(depositBankingRepository.GetAllActiveDepositors());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Currencies = depositBankingRepository.GetDepositInfoByName(investment.Currency).Currencies;
           
            return View(investment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InvestmentEditViewModel investment)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<InvestmentEditViewModel, Investment>()
                    .ForMember("FinalAmount", opt => opt.MapFrom(d => d.Amount + d.Duration * ((d.Rate / 100) / 12) * d.Amount))
                    .ForMember("BankState", opt => opt.MapFrom(d => "Active"))
                    .ForMember("DepositorState", opt => opt.MapFrom(d => "Active"))
                    );
                Investment newInvestment = Mapper.Map<InvestmentEditViewModel, Investment>(investment);

                depositBankingRepository.Update<Investment>(newInvestment);
                return RedirectToAction("Index");
            }
            return View(investment);
        }
        
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentDetailsViewModel>()
                .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
                .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name + " " + n.Depositor.Surname)));
            InvestmentDetailsViewModel investment = Mapper.Map<Investment, InvestmentDetailsViewModel>(depositBankingRepository.GetById<Investment>(id.Value));

            if (investment == null)
            {
                return HttpNotFound();
            }
            return View(investment);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Investment investment = depositBankingRepository.GetById<Investment>(id);

            if (investment == null)
            {
                return HttpNotFound();
            }
            else
            {
                investment.DestroyDate = DateTime.Now;
            }
            depositBankingRepository.Update<Investment>(investment);

            return RedirectToAction("Index");
        }       
       
    }
    
}
