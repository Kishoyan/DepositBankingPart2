﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PagedList;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;
using DepositBanking.Repository.Deposits;
using DepositBanking.Repositories;
using DepositBanking.Repository.DepositBankingEntityModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Extensons;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    public class DepositorsController : BaseController
    {
        public ActionResult Index(int? page)
        {
            Mapper.Initialize(b => b.CreateMap<Depositor, DepositorIndexViewModel>()
                .ForMember("FullName", opt => opt.MapFrom(n => n.Name+" "+n.Surname)));
            var depositors =
                Mapper.Map<IEnumerable<Depositor>, IEnumerable<DepositorIndexViewModel>>(depositBankingRepository.GetAll<Depositor>());
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(depositors.ToPagedList(pageNumber, pageSize));
        }
        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Depositor, DepositorDetailsViewModel>()
                .ForMember("State", opt => opt.MapFrom(n => n.DestroyDate == null ? "Active" : "Not Active")));
            DepositorDetailsViewModel depositor = Mapper.Map<Depositor, DepositorDetailsViewModel>(depositBankingRepository.GetById<Depositor>(id.Value));

            if (depositor == null)
            {
                return HttpNotFound();
            }
            return View(depositor);
        }
        
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,Address,Phone,Email,Birthday,Gender")] DepositorCreateViewModel depositor)
        {
            if (ModelState.IsValid)
            {
                var res = depositBankingRepository.GetDepositorByEmail(depositor.Email);
                if (res == null)
                {
                    Mapper.Initialize(b => b.CreateMap<DepositorCreateViewModel, Depositor>()
                     .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now)));

                    Depositor newDepositor = Mapper.Map<DepositorCreateViewModel, Depositor>(depositor);

                    depositBankingRepository.Create<Depositor>(newDepositor);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Message = "Depositor with this Email already exists.";
                    return View(depositor);
                }                
            }
            return View(depositor);
        }
        
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Depositor, DepositorEditViewModel>());
            DepositorEditViewModel depositor = Mapper.Map<Depositor, DepositorEditViewModel>(depositBankingRepository.GetById<Depositor>(id.Value));

            if (depositor == null)
            {
                return HttpNotFound();
            }
            return View(depositor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,Address,Phone,Email,Birthday,Gender,CreationDate")] DepositorEditViewModel depositor)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<DepositorEditViewModel, Depositor>());

                Depositor newdepositor = Mapper.Map<DepositorEditViewModel, Depositor>(depositor);

                depositBankingRepository.Update<Depositor>(newdepositor);
                return RedirectToAction("Index");
            }
            return View(depositor);
        }

        public ActionResult Invest(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DepositorInvestViewModel investment = new DepositorInvestViewModel();
            investment.DepositorId = id.Value;
            var depositor = depositBankingRepository.GetById<Depositor>(id.Value);
            if (depositor == null)
            {
                return HttpNotFound();
            }
            investment.Depositor= depositor;
            
            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Currencies = new List<CurrencyInfo>();
            return View(investment);
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Invest(DepositorInvestViewModel investment)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<DepositorInvestViewModel, Investment>()
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now))
                    .ForMember("FinalAmount", opt => opt.MapFrom(d => d.Amount + d.Duration * ((d.Rate / 100) / 12) * d.Amount))
                    .ForMember("BankState", opt => opt.MapFrom(d => "Active"))
                    .ForMember("DepositorState", opt => opt.MapFrom(d => "Active")));
                Investment newInvestment = Mapper.Map<DepositorInvestViewModel, Investment>(investment);

                depositBankingRepository.Create<Investment>(newInvestment);
                
                return RedirectToAction("Index");
            }
            return View(investment);
        }
        
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Depositor, DepositorDetailsViewModel>()
                .ForMember("State", opt => opt.MapFrom(n => n.DestroyDate == null ? "Active" : "Not Active")));
            DepositorDetailsViewModel depositor = Mapper.Map<Depositor, DepositorDetailsViewModel>(depositBankingRepository.GetById<Depositor>(id.Value));

            if (depositor == null)
            {
                return HttpNotFound();
            }
            return View(depositor);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Depositor depositor = depositBankingRepository.GetById<Depositor>(id);

            if (depositor == null)
            {
                return HttpNotFound();
            }
            else
            {
                depositor.DestroyDate = DateTime.Now;
            }

            depositBankingRepository.Update<Depositor>(depositor);

            var investments = depositBankingRepository.GetAll<Investment>().Where(d => d.DepositorId == id).ToList();
            if (investments == null)
            {
                return HttpNotFound();
            }
            else
            {
                foreach (var item in investments)
                {
                    item.DepositorState = "Not Active";
                    depositBankingRepository.Update<Investment>(item);
                }

            }
            return RedirectToAction("Index");
        }

       
    }
}
