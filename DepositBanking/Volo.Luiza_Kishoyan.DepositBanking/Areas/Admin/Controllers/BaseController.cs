﻿using AutoMapper;
using DepositBanking.Repositories;
using DepositBanking.Repository.Deposits;
using DepositBanking.Repository.DepositBankingEntityModels;
using System.Collections.Generic;
using System.Web.Mvc;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Extensons;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected IDepositBankingRepository depositBankingRepository;
           
        public BaseController()
            : this(new DepositBankingRepository())
        {   }

        public BaseController(IDepositBankingRepository repository)
        {
            depositBankingRepository = repository;
        }

        public ActionResult FillDepositType(string Name)
        {
            var depositInfo = depositBankingRepository.GetDepositInfoByName(Name);
           
            return Json(depositInfo, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<BankIndexViewModel> BanksDropdownListCreater(IEnumerable<Bank> bank)
        {
            Mapper.Initialize(b => b.CreateMap<Bank, BankIndexViewModel>());
            var Banks = Mapper.Map<IEnumerable<Bank>, IEnumerable<BankIndexViewModel>>(bank);
            var emptyBank = new BankIndexViewModel();

            return Banks.Append(emptyBank);
        }

        public IEnumerable<DepositorIndexViewModel> DepositorsDropdownListCreater(IEnumerable<Depositor> depositor)
        {
            Mapper.Initialize(b => b.CreateMap<Depositor, BankIndexViewModel>());
            var Depositors = Mapper.Map<IEnumerable<Depositor>, IEnumerable<DepositorIndexViewModel>>(depositor);
            var emptyDepositor = new DepositorIndexViewModel();

            return Depositors.Append(emptyDepositor);
        }

        public IEnumerable<DepositInfo> DepositsDropdownListCreater(IEnumerable<DepositInfo> deposits)
        {
            var emptyDeposit = new DepositInfo();
            deposits = deposits.Append(emptyDeposit);

            return deposits;
        }

        protected override void Dispose(bool disposing)
        {
            if (depositBankingRepository != null)
            {
                depositBankingRepository.Dispose();
                depositBankingRepository = null;
            }

            base.Dispose(disposing);
        }
    }
}