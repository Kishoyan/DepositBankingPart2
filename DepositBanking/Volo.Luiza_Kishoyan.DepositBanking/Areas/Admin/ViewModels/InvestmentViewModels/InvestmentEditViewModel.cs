﻿using DepositBanking.Repository.Deposits;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels
{
    public class InvestmentEditViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Type { get; set; }

        public double Rate { get; set; }

        public double Amount { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        public string Currency { get; set; }

        public int Duration { get; set; }

        [Required(ErrorMessage = "Select the Bank")]
        public int BankId { get; set; }

        [Required(ErrorMessage = "Select the Depositor")]
        public int DepositorId { get; set; }

        public double? FinalAmount { get; set; }

        public DateTime CreationDate { get; set; }

        public IEnumerable<BankIndexViewModel> Banks { get; set; }

        public IEnumerable<DepositorIndexViewModel> Depositors { get; set; }

        public IEnumerable<DepositInfo> Deposits { get; set; }

        public IEnumerable<CurrencyInfo> Currencies { get; set; }

    }
}