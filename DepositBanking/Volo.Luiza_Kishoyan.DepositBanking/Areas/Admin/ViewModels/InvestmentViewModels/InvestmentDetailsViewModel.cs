﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels
{
    public class InvestmentDetailsViewModel
    {
        public int Id { get; set; }
        
        public string Type { get; set; }

        public double Rate { get; set; }

        public double Amount { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
        
        public string Currency { get; set; }

        public int Duration { get; set; }

        public double? FinalAmount { get; set; }

        public string Bank { get; set; }

        public string Depositor { get; set; }

        public string BankState { get; set; }

        public string DepositorState { get; set; }

        public DateTime CreationDate { get; set; }
        
    }
}