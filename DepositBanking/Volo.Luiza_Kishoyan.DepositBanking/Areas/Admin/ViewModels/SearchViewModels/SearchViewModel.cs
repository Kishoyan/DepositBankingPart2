﻿using DepositBanking.Repository.Deposits;
using System;
using System.Collections.Generic;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.SearchViewModels
{
    public class SearchViewModel
    {

        public string Bank { get; set; }
        public string Depositor { get; set; }
        public string Deposit { get; set; }
        public double? AmountFrom { get; set; }
        public double? AmountTo { get; set; }
        public DateTime? StartDateFrom { get; set; }
        public DateTime? StartDateTo { get; set; }

        public virtual IEnumerable<BankIndexViewModel> Banks { get; set; }

        public virtual IEnumerable<DepositorIndexViewModel> Depositors { get; set; }

        public virtual IEnumerable<DepositInfo> Deposits { get; set; }
        

    }
}