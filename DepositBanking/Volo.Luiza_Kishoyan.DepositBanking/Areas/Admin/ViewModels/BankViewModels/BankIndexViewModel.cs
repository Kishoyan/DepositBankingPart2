﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels
{
    public class BankIndexViewModel
    {
        public BankIndexViewModel() {}

        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}