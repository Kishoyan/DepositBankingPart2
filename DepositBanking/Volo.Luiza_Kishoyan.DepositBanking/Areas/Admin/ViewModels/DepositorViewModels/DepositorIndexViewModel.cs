﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels
{
    public class DepositorIndexViewModel
    {
        public DepositorIndexViewModel() {}

        public int? Id { get; set; }

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public DateTime Birthday { get; set; }

    }
}