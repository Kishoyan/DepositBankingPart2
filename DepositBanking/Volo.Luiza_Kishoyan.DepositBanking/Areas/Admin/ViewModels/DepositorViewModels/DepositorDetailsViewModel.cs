﻿using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels
{
    public class DepositorDetailsViewModel
    {
        public DepositorDetailsViewModel()
        {
            Investments = new HashSet<Investment>();
            Banks = new HashSet<Bank>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Surname { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

        [Required]
        [StringLength(100)]
        public string Phone { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public string Gender { get; set; }

        public DateTime CreationDate { get; set; }

        public string State { get; set; }

        
        public virtual ICollection<Investment> Investments { get; set; }

        public virtual ICollection<Bank> Banks { get; set; }
    }
}