﻿$(function () {
    $("#Duration").change(function(){

        $("#FinalAmount").val(""); 
        var minDuration=$("#Duration").attr("min");
        var durationValue=$("#Duration").val();
        if(durationValue<minDuration){$("#Duration").val(minDuration);}
        var startDateValue=$("#StartDate").val();
        var parts =startDateValue.split('/');
        var newDate = new Date(parts[2],parts[1]-2,parts[0]);
        var month = newDate.getMonth()+1;
        newDate.setMonth(month+parseInt(durationValue));
        var newMonth=newDate.getMonth()+1;
        var day=newDate.getDate();
        if (newMonth < 10) {newMonth="0"+newMonth;}
        if (day < 10) {day="0"+day;}
        var endDateValue=day + '/' + newMonth + '/' +  newDate.getFullYear();
        $("#EndDate").val(endDateValue);
        });

        $("#Amount").change(function(){
        $("#FinalAmount").val("");
        var minAmount=$("#Amount").attr("min");
        var amount=$("#Amount").val();
        if(amount<minAmount){$("#Amount").val(minAmount);}
        });

    $("#StartDate").on("dp.change", function(e) {
        $("#FinalAmount").val("");         
        var durationValue=$("#Duration").val();
        var startDateValue=$("#StartDate").val();
        var parts =startDateValue.split('/');
        var newDate = new Date(parts[2],parts[1]-2,parts[0]);
        var month = newDate.getMonth()+1;
        newDate.setMonth(month+parseInt(durationValue));
        var newMonth=newDate.getMonth()+1;
        var day=newDate.getDate();
        if (newMonth < 10) {newMonth="0"+newMonth;}
        if (day < 10) {day="0"+day;}
        var endDateValue=day + '/' + newMonth + '/' +  newDate.getFullYear();
        $("#EndDate").val(endDateValue);
        });   
});
