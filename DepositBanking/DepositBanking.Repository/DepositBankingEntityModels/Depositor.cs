﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DepositBanking.Repository.DepositBankingEntityModels
{
    public class Depositor
    {
        public Depositor()
        {
            Investments = new List<Investment>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Surname { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

        [Required]
        [StringLength(100)]
        public string Phone { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? DestroyDate { get; set; }

        [Required]
        [StringLength(10)]
        public string Gender { get; set; }

        public virtual ICollection<Investment> Investments { get; set; }

        
    }
}
