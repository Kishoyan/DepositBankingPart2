﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepositBanking.Repository.DepositBankingEntityModels
{
    public class DepositBankingDBInitializer: DropCreateDatabaseAlways<DepositBankingEntityContext>
    {
        protected override void Seed(DepositBankingEntityContext context)
        {
            Bank b1 = new Bank
            {
                Name ="ACBA-CREDIT-AGRICOL BANK CJSC",
                OrganizationHead = "Hakob Andreasyan",
                Address = "Arami str. building No 82-84, territory No 87,89,99,100, Yerevan",
                Branch=58,
                Phone= "565 858",
                Fax= "54-34-85",
                Email= "acba@acba.am",
                SWIFT= "AGCAAM22",
                TPIN= "02520007",
                Logo="acba",
                CreationDate=DateTime.Now
            };
            Bank b2 = new Bank
            {
                Name = "Ameriabank CJSC",
                OrganizationHead = "Artak Hanesyan",
                Address = "Grigor Lusavorich str.9, Yerevan",
                Branch = 13,
                Phone = "58-99-06, 56-11-11, 56-43-19",
                Fax = "56-59-58",
                Email = "info@bank.am",
                SWIFT = "ARMIAM22",
                TPIN = "02502212",
                Logo = "ameria",
                CreationDate = DateTime.Now
            };
            Bank b3 = new Bank
            {
                Name = "Anelik Bank CJCS",
                OrganizationHead = "Nerses Karamanukyan",
                Address = "Vardanants str. 13, Yerevan",
                Branch = 13,
                Phone = "593 300, 593 301",
                Fax = "22-65-81",
                Email = "anelik@anelik.am",
                SWIFT = "ANIKAM22",
                TPIN = "00005409",
                Logo = "anelik",
                CreationDate = DateTime.Now
            };
            context.Banks.Add(b1);
            context.Banks.Add(b2);
            context.Banks.Add(b3);

            Depositor d1 = new Depositor
            {
                Name = "George",
                Surname = "L. Johnston",
                Address = "2232 Del Dew Drive Gaithersburg, MD 20877",
                Phone = "301-840-4143",
                Email = "GeorgeLJohnston@dayrep.com",
                Birthday = new DateTime(1982, 6, 13),
                Gender="Male",
                CreationDate= DateTime.Now
            };
            Depositor d2 = new Depositor
            {
                Name = "Eric",
                Surname = "T. Black",
                Address = "2255 Rafe Lane Durant, MS 39063",
                Phone = "3662-653-3946",
                Email = "EricTBlack@jourrapide.com",
                Birthday = new DateTime(1963, 3, 15),
                Gender = "Male",
                CreationDate = DateTime.Now
            };
            Depositor d3 = new Depositor
            {
                Name = "Agnes",
                Surname = "T. Smith",
                Address = "4573 Blackwell Street Wasilla, AK 99654",
                Phone = "907-354-1371",
                Email = "AgnesTSmith@teleworm.us",
                Birthday = new DateTime(1976, 5, 5),
                Gender = "Female",
                CreationDate = DateTime.Now
            };
            context.Depositors.Add(d1);
            context.Depositors.Add(d2);
            context.Depositors.Add(d3);

            base.Seed(context);
        }
    }
}
