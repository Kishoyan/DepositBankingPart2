﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DepositBanking.Repository.DepositBankingEntityModels
{
    public class Investment
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        public double Rate { get; set; }

        public decimal Amount { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(10)]
        public string Currency { get; set; }

        public int Duration { get; set; }

        public int BankId { get; set; }

        public int DepositorId { get; set; }

        [StringLength(50)]
        public string BankState { get; set; }

        [StringLength(50)]
        public string DepositorState { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? DestroyDate { get; set; }

        public decimal? FinalAmount { get; set; }

        public virtual Bank Bank { get; set; }

        public virtual Depositor Depositor { get; set; }
    }
}
