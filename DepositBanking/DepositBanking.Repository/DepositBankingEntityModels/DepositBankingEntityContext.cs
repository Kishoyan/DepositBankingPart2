﻿namespace DepositBanking.Repository.DepositBankingEntityModels
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    
        public class DepositBankingEntityContext : DbContext
        {
        public DepositBankingEntityContext()
            : base("name=DepositBankingDB")
        {
        }

            public virtual DbSet<Bank> Banks { get; set; }
            public virtual DbSet<Depositor> Depositors { get; set; }
            public virtual DbSet<Investment> Investments { get; set; }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {

            }
        }
 

}