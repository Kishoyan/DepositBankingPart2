﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace DepositBanking.Repository.Deposits
{
    public class DepositInfo
    {
        public string Type { get; }
        public double Rate { get; }
        public int MinDuration { get; }
        public int MaxDuration { get; }
        public string MinStartDate { get; }
        public string MinEndDate { get; }

        public IEnumerable<CurrencyInfo> Currencies;

        public DepositInfo() { }
        public DepositInfo(string type, double rate, int minDuration, int maxDuration, DateTime minStartDate, DateTime minEndDate, List<CurrencyInfo> currencies)
        {
            Type = type;
            Rate = rate;
            MinDuration = minDuration;
            MaxDuration = maxDuration;
            MinStartDate = minStartDate.ToString("dd'/'MM'/'yyyy");
            MinEndDate = minEndDate.ToString("dd'/'MM'/'yyyy");
            Currencies = currencies;
        }
    }
}