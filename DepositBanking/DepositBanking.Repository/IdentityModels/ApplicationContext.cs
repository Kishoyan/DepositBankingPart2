﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DepositBanking.Repository.IdentityModels
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("IdentityDb") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
    }
}
